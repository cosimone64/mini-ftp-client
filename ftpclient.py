#Author: Cosimo Agati

import socket
import os


class MyFtpClient:
    def __init__(self, work_dir):
        """Initializes all data structures used by our client."""
        if not os.path.exists(work_dir):
            os.mkdir(work_dir)
        os.chdir(work_dir)
        self.client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.datasock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.main_host = ''
        self.data_host = ''
        self.is_connected = False
        self.reset_log()
        self.reset_dataconn()

#Metodi aggiuntivi per implementare quelli richiesti
    def reset_log(self):
        """Resets all log status."""
        self.last_message = 'No messages received so far\r\n'
        self.message_log = ''
        self.transfer_mode = 'ASCII'

    def reset_dataconn(self):
        """Rets data connection status."""
        self.datasock.close()
        self.is_dataconn_set = False
        self.is_server_passive = False
        self.data_port = 0

    def send_msg(self, message):
        """Sends a message to the server and adds it to the log."""
        message = message + '\r\n'
        self.client_sock.send(message)
        self.update_log('> ', message)

    def recv_msg(self):
        """Receives a message from the server and saves it in the log."""
        server_response = self.get_server_response()
        self.update_state(server_response)
        if server_response.startswith('5'):
            return self.reset_dataconn()
        return server_response

    def get_server_response(self):
        """Receives a single message from server."""
        response = ''
        for i in range(0, 4):
            response = response + self.client_sock.recv(1)
        if response.endswith('-'):
            response = self.parse_multiline_response(response)
        while not response.endswith('\r\n'):
            response = response + self.client_sock.recv(1)
        return response

    def parse_multiline_response(self, response):
        """In case of a multiline message, look for the last line."""
        last_line_begin = '\r\n' + response[:3] + ' '
        while not response.endswith(last_line_begin):
            response = response + self.client_sock.recv(1)
        return response

    def update_state(self, response):
        """Updates log status when receiving a message."""
        self.update_log('< ', response)
        self.last_message = response

    def update_log(self, starting_string, message):
        """Updates the log with the message passed as parameter."""
        new_log_entry = starting_string + message
        self.message_log = self.message_log + new_log_entry

    def create_dataconn(self):
        """Initiates data connection."""
        if self.is_server_passive:
            self.datasock.connect((self.data_host, self.data_port))
        else:
            self.create_active_dataconn()

    def create_active_dataconn(self):
        """Accepts a connection in case of an active server."""
        tmp_sock, tmp_addr = self.datasock.accept()
        self.datasock.close()
        self.datasock = tmp_sock

    def send_data(self, message):
        """Sends message through the data connection."""
        self.datasock.sendall(message)
        self.reset_dataconn()
        self.recv_msg()

    def recv_data(self):
        """Receives data through the data connection."""
        total_data = ''
        tmp_data = self.datasock.recv(1024)
        while tmp_data:
            total_data = total_data + tmp_data
            tmp_data = self.datasock.recv(1024)
        self.reset_dataconn()
        self.recv_msg()
        return total_data

    def isconnected(self):
        """Returns True if the client is connected, False otherwise."""
        return self.is_connected

    def lastcode(self):
        """Returns the last code received by the server, possibly None."""
        if not self.is_connected:
            return
        return int(self.last_message[:3])

    def lastmessage(self):
        """Returns the last message received by the server, possibly None."""
        return self.last_message

    def log(self):
        """Returns a string containing the message log."""
        return self.message_log

    def connect(self, host, port):
        """Connects to a server located at host:port."""
        if self.is_connected:
            raise TypeError('Already connected to server')
        self.client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_sock.connect((host, port))
        self.recv_msg()
        self.reset_log()
        self.main_host = self.data_host = self.client_sock.getsockname()[0]
        self.is_connected = True

    def user(self, user):
        """Sends USER command."""
        if not self.is_connected:
            raise Exception('Executed USER command while not connected')
        self.send_msg('USER ' + user)
        self.recv_msg()

    def password(self, password):
        """Sends PASS command."""
        if not self.is_connected:
            raise Exception('Executed PASS command while not connected')
        self.send_msg('PASS ' + password)
        self.recv_msg()

    def system(self):
        """Sends SYST command."""
        if not self.is_connected:
            raise Exception('Executed SYST command while not connected')
        self.send_msg('SYST')
        self.recv_msg()

    def disconnect(self):
        """Sends QUIT command and disconnects from the server."""
        if not self.is_connected:
            raise Exception('Executed QUIT command while not connected')
        self.send_msg('QUIT')
        self.recv_msg()
        self.client_sock.close()
        self.reset_dataconn()
        self.is_connected = False

    def pwd(self):
        """Sends PWD command and saves the response for later use."""
        if not self.is_connected:
            raise Exception('Executed PWD command while not connected')
        self.send_msg('PWD')
        self.recv_msg()
        self.curr_dir = self.last_message.split()[1]

    def lpwd(self):
        """Returns the current directory on the local machine."""
        if not self.is_connected:
            raise Exception('Called lpwd() while not connected')
        return os.getcwd()

    def port(self):
        """Handles PORT command."""
        if not self.is_connected:
            raise Exception('Executed PORT command while not connected')
        self.datasock = self.get_active_datasock()
        self.data_port = self.datasock.getsockname()[1]
        x, y = self.get_active_xy()
        address = self.data_host.replace('.', ',')
        self.send_msg('PORT ' + address + ',' + x + ',' + y)
        if self.recv_msg():
            self.is_dataconn_set = True
            self.is_server_passive = False

    def get_active_datasock(self):
        """In case of an active server, a socket is created to listen."""
        datasock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.data_host = self.main_host
        datasock.bind((self.data_host, 0))
        datasock.listen(1)
        return datasock

    def get_active_xy(self):
        """Based on the port used, gets x and y values."""
        x = self.data_port / 256
        y = self.data_port % 256
        return str(x), str(y)

    def pasv(self):
        if not self.is_connected:
            raise Exception('Executed PASV command while not connected')
        self.datasock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.send_msg('PASV')
        response = self.recv_msg()
        if response:
            self.data_port = self.get_pasv_params(response)
            if not self.data_port:
                self.reset_dataconn()
                return
            self.is_dataconn_set = True
            self.is_server_passive = True

    def get_pasv_params(self, data):
        """Parses the passive port to get parameters."""
        start = data.find('(')
        end = data.find(')')
        if start < 0 or end < 0:
            return
        elements = data[(start + 1):end].split(',')
        if len(elements) != 6:
            return
        for value in elements:
            if not value.isdigit():
                return
        self.data_host = '.'.join(elements[:4])
        port = int(elements[4]) * 256 + int(elements[5])
        if not 0 < port < 65536:
            return
        return port

    def data(self):
        """Returns info about the data connection, None if not active."""
        if not self.is_connected:
            raise Exception('Called data() while not connected')
        if not self.is_dataconn_set:
            return
        return (self.is_server_passive, self.data_host, self.data_port)

    def cd(self, dir):
        """Sends CWD command."""
        if not self.is_connected:
            raise Exception('Executed CWD command while not connected')
        self.send_msg('CWD ' + dir)
        self.recv_msg()

    def lcd(self, dir):
        """Changes directory on the local machine."""
        if not self.is_connected:
            raise Exception('Called lcd[up]() while not connected')
        try:
            os.chdir(dir)
        except OSError:
            raise TypeError('Directory ' + dir + ' does not exist')

    def list(self, dir):
        """Sends LIST command."""
        if not self.is_connected or not self.is_dataconn_set:
            raise Exception('Incorrect connection staus when calling LIST')
        self.send_msg('LIST ' + dir)
        self.create_dataconn()
        if self.recv_msg():
            return self.recv_data()

    def cdup(self):
        """Sends CDUP command."""
        if not self.is_connected:
            raise Exception('Executed CDUP command while not connected')
        self.send_msg('CDUP')
        self.recv_msg()

    def lcdup(self):
        """Changes to the parent directory on the local machine."""
        self.lcd('..')

    def ascii(self):
        """Sets transfer type to ASCII."""
        if not self.is_connected:
            raise Exception('Executed TYPE command while not connected')
        self.send_msg('TYPE A')
        self.recv_msg()
        self.transfer_mode = 'ASCII'

    def binary(self):
        """Sets transfer type to IMAGE."""
        if not self.is_connected:
            raise Exception('Executed TYPE command while not connected')
        self.send_msg('TYPE I')
        self.recv_msg()
        self.transfer_mode = 'IMAGE'

    def mode(self):
        """Returns current data transfer mode."""
        if not self.is_connected:
            raise Exception('Called mode() while not connected')
        return self.transfer_mode

    def get(self, remote, local):
        """Handles RETR command."""
        if not self.is_connected or not self.is_dataconn_set:
            raise TypeError('Incorrect connection status when calling RETR')
        self.send_msg('RETR ' + remote)
        if self.is_server_passive:
            self.create_dataconn()
            if self.recv_msg():
                self.recv_file(local)
        elif self.recv_msg():
            self.create_dataconn()
            self.recv_file(local)

    def recv_file(self, local):
        """Writes to 'local' file all data received."""
        data = self.recv_data()
        if data:
            retr_file = open(local, self.get_file_mode('w'))
            retr_file.write(data)

    def get_file_mode(self, mode):
        """Determines the current file transfer mode: ascii or binary."""
        if self.transfer_mode.startswith('I'):
            mode = mode + 'b'
        return mode

    def put(self, local, remote):
        """Handles STOR command."""
        if not self.is_connected or not self.is_dataconn_set:
            raise TypeError('Incorrect connection status when calling STOR')
        self.send_file(local, remote)

    def send_file(self, local, remote):
        """Sends 'local' file, sending the command STOR remote."""
        try:
            stor_file = open(local, self.get_file_mode('r'))
        except IOError:
            self.is_dataconn_set = False
            raise TypeError('File ' + local + ' does not exist')
        self.send_msg('STOR ' + remote)
        if self.is_server_passive:
            self.create_dataconn()
            if self.recv_msg():
                self.send_data(stor_file.read())
        elif self.recv_msg():
            self.create_dataconn()
            self.send_data(stor_file.read())
